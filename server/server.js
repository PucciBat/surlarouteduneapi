var express = require('express')
const axios = require('axios')
var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/'))

app.get('/form', function (req, res) {
    res.sendfile('./form.html');
});


app.post('/form', urlencodedParser, function (req, res) {//pick up data from form 
    res.send('data sent to API'); // send reponse
    console.log('blabla');

    axios.post('http://localhost:8080/user/new', req.body)//sent data tu API (sent form req.body)
        .then(function (res) {
            console.log('OK');
        })
        .catch(function (error) {
            console.log(error);
        });
});

app.get('/table', function (req, res) { // call site table

    axios.get('http://localhost:8080/user')//call data json from API
        .then(function (resu) {
            console.log('data back');
            console.log(resu)
            res.render('table', { 'users': resu.data });
        })
        .catch(function (error) {
            console.log(error);
        });

});

//app.get('/table', function (req, res) {
//    
//    axios.delete("http://localhost:8080/user/:name")
//        .then(function (resp) {
//            console.log(resp)
//        })
//        .catch(function (err) {
//            console.log(err);
//        });
//});

app.listen(8082);